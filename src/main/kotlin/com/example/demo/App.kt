package com.example.demo

import org.jetbrains.exposed.spring.SpringTransactionManager
import org.jetbrains.exposed.sql.*
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.annotation.Profile
import org.springframework.context.support.beans
import org.springframework.jdbc.core.JdbcOperations
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.support.TransactionTemplate
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@SpringBootApplication
class App

fun main(args: Array<String>) {
  // runApplication<App>(*args)
  SpringApplicationBuilder()
      .sources(App::class.java)
      .initializers(beans(), router())
      .run(*args)
}

fun beans() = beans {
  bean {
    SpringTransactionManager(ref())
  }
  bean {
    ApplicationRunner {
      val customerService = ref<CustomerService>()
      arrayOf("C1", "C2", "C3")
          .map { Customer(name = it) }
          .forEach { customerService.insert(it) }
      customerService.all().forEach { println(it) }
    }
  }
}

fun router() = beans {
  bean {
    router {
      val customerService = ref<CustomerService>()
      GET("/customers") { ServerResponse.ok().body(Flux.fromIterable(customerService.all())) }
      GET("/customer/{id}") {
        val errMsg = "Invalid data type for `/customer/{id}`. `String` doesn't conform to the expect type `Long`"
        val id = it.pathVariable("id").toLongOrNull() ?: -1L
        when (id) {
          -1L -> ServerResponse.badRequest().body(Mono.just(Error(errMsg)))
          else -> {
            val customer = customerService.findById(id)
            when (customer) {
              null -> ServerResponse.notFound().build()
              else -> ServerResponse.ok().body(Mono.just(customer))
            }
          }
        }
      }
    }
  }
}

data class Error(val error: String)

data class Customer(val name: String, val id: Long? = null)

object Customers : Table() {
  val id = long("id").autoIncrement().primaryKey()
  val name = varchar("name", 255)
}

interface CustomerService {
  fun all(): Collection<Customer>
  fun findById(id: Long): Customer?
  fun insert(c: Customer)
}

@Service
@Transactional
class ExposedCustomerSerivce(private val transactionTemplate: TransactionTemplate)
  : CustomerService, InitializingBean {

  override fun afterPropertiesSet() {
    transactionTemplate.execute { SchemaUtils.create(Customers) }
  }

  override fun all(): Collection<Customer> = Customers
      .selectAll()
      .map { Customer(it[Customers.name], it[Customers.id]) }

  override fun findById(id: Long): Customer? = Customers
      .select { Customers.id.eq(id) }
      .map { Customer(it[Customers.name], it[Customers.id]) }
      .firstOrNull()

  override fun insert(c: Customer) {
    Customers.insert { it[Customers.name] = c.name }
  }
}

@Profile("jdbc")
@Service
@Transactional
class JdbcCustomerService(private val jdbcOperations: JdbcOperations) : CustomerService {

  override fun all(): Collection<Customer> = jdbcOperations
      .query("select * from CUSTOMERS") { rs, _ ->
        Customer(rs.getString("NAME"), rs.getLong("ID"))
      }

  override fun findById(id: Long): Customer? = jdbcOperations
      .queryForObject("select * from CUSTOMERS where ID = ?") { rs, _ ->
        Customer(rs.getString("NAME"), rs.getLong("ID"))
      }

  override fun insert(c: Customer) {
    jdbcOperations.execute("insert into CUSTOMERS(NAME) values(?)") {
      it.setString(1, c.name)
      it.execute()
    }
  }
}
